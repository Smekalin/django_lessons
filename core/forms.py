from django import forms
from django.contrib.auth.models import User

class RegisterForm(forms.Form):
    username = forms.CharField(max_length=128)
    password = forms.CharField(max_length=128, widget=forms.PasswordInput)
    email = forms.CharField(max_length=128)

    def registration(self):
        print('User registraion')
        User.objects.create_user(username=self.cleaned_data['username']
                                 , password=self.cleaned_data['password']
                                 , email=self.cleaned_data['email'])
