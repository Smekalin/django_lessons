from django.views.generic import FormView
from django.contrib.auth.models import User
from django.shortcuts import resolve_url
from .forms import RegisterForm

class Registration(FormView):
    form_class = RegisterForm
    model = User
    fields = ['username', 'password', 'email']
    template_name = 'register.html'

    def form_valid(self, form):
        print('form_valid')
        form.registration()
        return super(Registration, self).form_valid(form)

    def get_success_url(self):
        return resolve_url('login')