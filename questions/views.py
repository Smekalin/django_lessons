from django.views.generic import ListView, DetailView, CreateView
from .models import Question
from .forms import QuestionListForm
from django.shortcuts import resolve_url


class QuestionCreate(CreateView):
    model = Question
    fields = ['text', 'tittle']
    template_name = 'questions/question_create.html'

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super(QuestionCreate, self).form_valid(form)

    def get_success_url(self):
        return resolve_url('question_detail', pk=self.object.pk) # question_detail - именнованный url !!!


class QuestionDetail(DetailView):
    model = Question
    template_name = 'questions/question_detail.html'
    context_object_name = 'question'


class QuestionList(ListView):
    model = Question
    template_name = 'questions/question_list.html'

    def dispatch(self, request, *args, **kwargs):
        self.form = QuestionListForm(request.GET)
        self.form.is_valid()
        return super(QuestionList, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        queryset = Question.objects.filter(author=self.request.user)

        if self.form.cleaned_data.get('search_field'):
            queryset = queryset.filter(tittle__icontains=self.form.cleaned_data['search_field'])
        if self.form.cleaned_data.get('sort_field'):
            queryset = queryset.order_by(self.form.cleaned_data['sort_field'])
        return queryset

    def get_context_data(self, **kwargs):
        context = super(QuestionList, self).get_context_data(**kwargs)
        context['form'] = self.form
        return context

