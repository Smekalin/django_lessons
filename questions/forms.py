# coding: utf-8

from django import forms
from .models import Question
# Используется для валидации пользовательских данных
class QuestionListForm(forms.Form):
    search_field = forms.CharField(required=False)
    sort_field = forms.ChoiceField(choices=(('id','ID'), ('public_date', u'Дата создания')), required=False)

    # def clean(self):
    #     raise forms.ValidationError(u'Нет')

    # def clean_search_field(self):
    #     search = self.cleaned_data.get('search_field')
    #     raise forms.ValidationError(u'Нет')
    #     return search


class QuestionForm(forms.Form):

    tittle = forms.CharField(max_length=256)
    text = forms.CharField(widget=forms.Textarea)
