from django.db import models
from django.conf import settings

class Question(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL)
    tittle = models.CharField(max_length=256)
    text = models.TextField()
    public_date = models.DateField(auto_now_add=True)
    file = models.FileField(blank=True)
    image = models.ImageField(blank=True)
    def __str__(self):
        return self.tittle

    class Meta:
        verbose_name = u'Вопрос'
        verbose_name_plural = u'Вопросы'
        ordering = ('-public_date', )


class Answer(models.Model):
    question = models.ForeignKey(Question, related_name='answer')
    text = models.TextField()
    pub_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return ('{} {}...').format(self.question.id, self.text[:10])