from django.conf.urls import url

from questions.views import QuestionList, QuestionDetail, QuestionCreate

urlpatterns = [
    url(r'^(?P<pk>\d+)/$', QuestionDetail.as_view(), name='question_detail'),
    url(r'^list/$', QuestionList.as_view(), name='question_list'),
    url(r'^create/$', QuestionCreate.as_view(), name='question_create'),
]