# -*- coding:utf-8 -*-

from django.conf.urls import url
from news import views
from .views import NewsView

urlpatterns = [
    url(r'^(?P<pk>\d+)/$', NewsView.as_view(), name='news_detail'),
    url(r'^(?P<news_id>\d+)/like/$', views.show_news),
]
