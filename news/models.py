# coding: utf-8

from django.db import models


class Article(models.Model):
    tittle = models.CharField(max_length=255)
    text = models.TextField()
    pub_date = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.tittle